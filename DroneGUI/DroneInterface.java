package DroneSimulationGUI;




import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class DroneInterface extends Application  {

	private ConsoleCanvas mc;										// canvas for drawing it
	private AnimationTimer timer;								// timer used for animation
	private VBox rtPane;										// vertical box for putting info
	private DroneArena DA;                                      // drone arena
	
	
		
	

	/**
	 * initialise the Arena
	 */
	public DroneInterface() {
					
		DA = new DroneArena(400, 500);       // initialise arena
		
	}

	/**
	 * function to show a box ABout programme
	 */
	private void showAbout() {
	    Alert alert = new Alert(AlertType.INFORMATION);				// define what box is
	    alert.setTitle("About");									// say is About
	    alert.setHeaderText(null);
	    alert.setContentText("Drone Simulation");			// give text
	    alert.showAndWait();										// show box and wait for user to close
	}
	

	/**
	 * loads save file
	 */
	public void openFile() {
		File workingDirectory = new File(System.getProperty("user.dir"));
		JFileChooser chooser = new JFileChooser(workingDirectory); // makes the user start at the right directory
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File selFile =  chooser.getSelectedFile();
			System.out.println("You chose to open this file: " + selFile.getName());
			if(selFile.isFile()) {// exits and is a file
				try {
					  FileInputStream fileStream = new FileInputStream(selFile);
					  ObjectInputStream objStream = new ObjectInputStream(fileStream);
					  ObjectInputStream input = new ObjectInputStream(fileStream);
					 
					  
		    	       DroneArena DA = (DroneArena)input.readObject(); // reads object in file
		    	       
		    	       objStream.close();
		    	       input.close();
		    	      
		    	      }
		    	      
		    	   
		    	     
		    	     catch (IOException | ClassNotFoundException e) {
		    	      System.out.println("An error occurred.");               
		    	      e.printStackTrace();
		    	    }
			}
			
		}
	}
	
	
	/**
	 * saves the drone Arena into a file
	 */
	public void saveFile() {
		File workingDirectory = new File(System.getProperty("user.dir"));
		JFileChooser chooser = new JFileChooser(workingDirectory);
		int returnVal = chooser.showSaveDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File selFile =  chooser.getSelectedFile();
			File currDir = chooser.getCurrentDirectory();
			try {
				FileOutputStream outStream = new FileOutputStream(selFile);
				ObjectOutputStream outObjectStream = new ObjectOutputStream (outStream);
				outObjectStream.writeObject(DA);                        // writes the drone arena object into file
				outStream.close();
				outObjectStream.close();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		
			
			
		}
	}
	
	


	

	 /**
	  * set up the mouse event - when mouse pressed, put ball there
	  * @param canvas
	  */
	void setMouseEvents (Canvas canvas) {
	       canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, 		// for MOUSE PRESSED event
	    	       new EventHandler<MouseEvent>() {
	    	           @Override
	    	           public void handle(MouseEvent e) {
	    	        	   DA.putDrone((int) e.getX(), (int) e.getY());
	  		            	DA.drawWorld(mc);							// redraw world
	  		            	drawStatus();
    	           }
	    	       });
	}
	/**
	 * set up the menu of commands for the GUI
	 * @return the menu bar
	 */
	MenuBar setMenu() {
		MenuBar menuBar = new MenuBar();						// create main menu
		Menu mFile = new Menu("File");							// add File main menu
		MenuItem mExit = new MenuItem("Exit");					// whose sub menu has Exit
		MenuItem mSave = new MenuItem("Save");
		MenuItem mLoad = new MenuItem("Load");
		mExit.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {					// action on exit is
	        	timer.stop();									// stop timer
		        System.exit(0);									// exit program
		    }
		});
		mFile.getItems().addAll(mExit);							// add exit to File menu
		
		mSave.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {					// action on exit is
		    	timer.stop();
	        	saveFile();
		        								
		    }
		});
		mFile.getItems().addAll(mSave);
		
		
		mLoad.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {					// action on exit is
	        	timer.stop();									// stop timer
	        	
	        	openFile();
		       								// exit program
		    }
		});
		mFile.getItems().addAll(mLoad);	
		
		Menu mHelp = new Menu("Help");							// create Help menu
		MenuItem mAbout = new MenuItem("About");				// add About sub men item
		mAbout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
            	showAbout();									// and its action to print about
            }	
		});
		
		mHelp.getItems().addAll(mAbout);						// add About to Help main item
		
		menuBar.getMenus().addAll(mFile, mHelp);				// set main menu with File, Help
		return menuBar;											// return the menu
	}

	/**
	 * set up the horizontal box for the bottom with relevant buttons
	 * @return
	 */
	private HBox setButtons() {
	    Button btnStart = new Button("Start");					// create button for starting
	    btnStart.setOnAction(new EventHandler<ActionEvent>() {	// now define event when it is pressed
	        @Override
	        public void handle(ActionEvent event) {
	        	timer.start();									// its action is to start the timer
	       }
	    });

	    Button btnStop = new Button("Pause");					// now button for stop
	    btnStop.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	           	timer.stop();									// and its action to stop the timer
	       }
	    });
	    

	    Button btnAdd = new Button("Add Drone");                // button for adding drone				
	    btnAdd.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	        	DA.addDrone();                 // adds drone
	        	drawWorld();                 // draws world so drone shows up
	        	
	        	
	       }
	    });
	    Button btnBlock = new Button("Add Block");			// button for adding block
	    btnBlock.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	        	DA.addBlock();             // adds block
	        	drawWorld();             //draws world so drone shows up
	        	
	        	
	       }
	    });
	    Button btnRunner = new Button("Add Runner");			// button for adding block
	    btnRunner.setOnAction(new EventHandler<ActionEvent>() {
	        @Override
	        public void handle(ActionEvent event) {
	        	DA.addRunner();             // adds block
	        	drawWorld();             //draws world so drone shows up
	        	
	        	
	       }
	    });

	    														// now add these buttons + labels to a HBox
	    return new HBox(new Label("Run: "), btnStart, btnStop, btnAdd,btnBlock, btnRunner);
	}

	
	/**
	 * show where ball is, in pane on right
	 */
	public void drawStatus() {
		rtPane.getChildren().clear();	// clear rtpane
		ArrayList<String> allEs = DA.describeEntities(); 
		for (String s : allEs) {   // goes
			Label l = new Label(s); 		// turn description into a label
			rtPane.getChildren().add(l);	// add label	
		}
	}
	
	/**
	 * draws the world
	 */
	public void drawWorld() {
		mc.clearCanvas();                           
		DA.drawWorld(mc); 
	}
	/**
	 * main function ... sets up canvas, menu, buttons and timer
	 */
	@Override
	public void start(Stage stagePrimary) throws Exception {
		stagePrimary.setTitle("Drone Simulation");
	    BorderPane bp = new BorderPane();
	    bp.setPadding(new Insets(10, 20, 10, 20));

	    bp.setTop(setMenu());											// put menu at the top

	    Group root = new Group();										// create group with canvas
	    Canvas canvas = new Canvas( DA.getXSize(), DA.getYSize() );
	    root.getChildren().add( canvas );
	    bp.setLeft(root);												// load canvas to left area

	    mc = new ConsoleCanvas(canvas.getGraphicsContext2D(), DA.getXSize(), DA.getYSize());

//	    gc = canvas.getGraphicsContext2D();								// context for drawing

	    rtPane = new VBox();											// set vBox on right to list items
		rtPane.setAlignment(Pos.TOP_LEFT);								// set alignment
		rtPane.setPadding(new Insets(2, 5, 5, 2));						// padding
 		bp.setRight(rtPane);											// add rtPane to borderpane right
		  
	    bp.setBottom(setButtons());										// set bottom pane with buttons

	    setMouseEvents(canvas);											// set up mouse events
	    DA.drawWorld(mc);													// draw world with ball there
	    
	    timer = new AnimationTimer() {									// set up timer
	        public void handle(long currentNanoTime) {					// and its action when on
	        		DA.updateArena();
		            DA.drawWorld(mc);										// redraw the world
		            drawStatus();										// indicate where ball is
	        }
	    };

	    Scene scene = new Scene(bp, 600, 600);							// set overall scene
        bp.prefHeightProperty().bind(scene.heightProperty());
        bp.prefWidthProperty().bind(scene.widthProperty());

        stagePrimary.setScene(scene);
        stagePrimary.show();
	  
	}

	/**
	 * main ... just instigate the GUI...
	 * @param args
	 */
	public static void main(String[] args) {
		Application.launch();
	
	    			// launch the GUI
	}

	
}
