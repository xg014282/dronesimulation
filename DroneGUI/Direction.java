package DroneSimulationGUI;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;




public enum Direction {

	North,South, East,West,NorthEast,SouthWest,SouthEast,NorthWest;
	/**
	 * chooses the next direction
	 */
	public Direction next() {
		return values()[(ordinal() + 1) % values().length];
	}
	/**
	 * chooses a random direction
	 */
	public static Direction getRandomDirection() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
	/**
	 * used to determine movement when hitting a wall
	 * returns direction
	 */
	public Direction lowerWall() {
	    int ranCase = ThreadLocalRandom.current().nextInt(0,3);   // randomly selects which direction the drone will go away from wall
	    Direction way = null;
	    switch(ranCase) {
	    case 0:
	    	way = North;
	    	break;
	    case 1:
	    	way = NorthWest;
	    	break;
	    case 2:
	    	way = NorthEast;
	    	break;
	    	
	    }
	    
	    return way;
	}
	public Direction leftWall() {
	    int ranCase = ThreadLocalRandom.current().nextInt(0,3);
	    Direction way = null;
	    switch(ranCase) {
	    case 0:
	    	way = East;
	    	break;
	    case 1:
	    	way = SouthEast;
	    	break;
	    case 2:
	    	way = NorthEast;
	    	break;
	    	
	    }
	    
	    return way;
	}
	public Direction rightWall() {
	    int ranCase = ThreadLocalRandom.current().nextInt(0,3);
	    Direction way = null;
	    switch(ranCase) {
	    case 0:
	    	way = West;
	    	break;
	    case 1:
	    	way = NorthWest;
	    	break;
	    case 2:
	    	way = SouthWest;
	    	break;
	    	
	    }
	    
	    return way;
	}
	public Direction upperWall() {
	    int ranCase = ThreadLocalRandom.current().nextInt(0,3);
	    Direction way = null;
	    switch(ranCase) {
	    case 0:
	    	way = South;
	    	break;
	    case 1:
	    	way = SouthWest;
	    	break;
	    case 2:
	    	way = SouthEast;
	    	break;
	    	
	    }
	    
	    return way;
	}
	
	
}