package DroneSimulationGUI;

import java.util.concurrent.ThreadLocalRandom;





public class Drone extends Entity {
	 private int count = 0;                                    // used for timing
	 private int moveTime = ThreadLocalRandom.current().nextInt(50,230);        // used to randomise time


	 /**
	  *  @param ix
	  *  @param iy
	  */
     Drone(double ix, double iy) {
		 super(ix, iy);
		 rad = 20;
		 spd = 1;
	 }


     /** 
 	 * used to make move drone based on direction
 	 */
	@Override
	protected void adjustEntity() {
		// TODO Auto-generated method stub
		count++;
		switch(way) {       // changes x and y based on direction
		case South:
			y += spd ;	
			break;
		case SouthEast:
			y += spd;
			x += spd;
			break;
		case West:	
			x -= spd ;
			break;
		case SouthWest:
			y += spd;
			x -= spd;
			break;
		case NorthEast:
			y -= spd;
			x += spd;
			break;
		case NorthWest:
			y -= spd;
			x -= spd;
			break;
		case East:	
			x += spd ;
			break;
		case North:
			y -= spd ;	
			break;
		}
		// randomises time and direction
		if(count > moveTime) {                         
			way = way.getRandomDirection();
		    moveTime = ThreadLocalRandom.current().nextInt(50,230);;    // makes it so the time between changing directions is random
			count = 0;
		}

	}
	
	/** 
	 * used to make sure drone is in arena
	 * @param ax
	 * @param ay
	 */
	@Override
	protected void checkEntity(int ax, int ay) {
		// TODO Auto-generated method stub
		if(x + rad>= ax) way = way.rightWall();               // will change direction based on which wall hit
     	if(y + rad>= ay) way = way.lowerWall();
		if(y <= 1 + rad) way = way.upperWall();
		if(x <= 1 + rad) way = way.leftWall();
		
	}

	/**
	 * return string defining entity ... here as Drone
	 */
	protected String getStrType() {
		return "Drone ";
	}
	
	
	/**
	 * used to move the drone after collision
	 */
	@Override
	protected void opposite() {
		// TODO Auto-generated method stub
		switch(way) {          // the directions are directly opposite so the drone moves away from collision
		case South:
			way = way.North;	
			break;
		case SouthEast:
			way = way.NorthWest;	
			break;
		case West:	
			way = way.East;	
			break;
		case SouthWest:
			way = way.NorthEast;	
			break;
		case NorthEast:
			way = way.SouthWest;	
			break;
		case NorthWest:
			way = way.SouthEast;	
			break;
		case East:	
			way = way.West;	
			break;
		case North:
			way = way.South;		
			break;
		}
	}
	
	/**
	 * used to draw the drone
	 * 
	 */
	@Override
	protected void drawEntity(ConsoleCanvas mc) {
		// TODO Auto-generated method stub
		mc.showEntity(x, y, rad, 'y'); // yellow drone is added
	}
	
	
	
	
	

	

	
	
	
		

}
