package DroneSimulationGUI;

public class Block extends Entity {

	
	/**
	 * @param ix
	 * @param iy
	 */
	Block(double ix, double iy) {
		super(ix, iy);
		rad = 30;
	}

	@Override
	protected void opposite() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void adjustEntity() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void checkEntity(int ax, int ay) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void drawEntity(ConsoleCanvas mc) {
		mc.showEntity(x, y, rad, 'b');   // draws blue block

	}
	
	/**
	 * return string defining entity ... here as Block
	 */
	protected String getStrType() {
		return "Block ";
	}
	
	
	
	

	

}
