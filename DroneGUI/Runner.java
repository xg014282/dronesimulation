package DroneSimulationGUI;

public class Runner extends Entity {
	
	
	Runner(double ix, double iy) {
		super(ix, iy);
		rad = 10;
		spd = 2;
		
	}
	
	


	@Override
	protected void opposite() {
		// TODO Auto-generated method stub
		switch(way) {          // the directions are directly opposite so the drone moves away from collision
		case South:
			way = way.North;	
			break;
		case SouthEast:
			way = way.NorthWest;	
			break;
		case West:	
			way = way.East;	
			break;
		case SouthWest:
			way = way.NorthEast;	
			break;
		case NorthEast:
			way = way.SouthWest;	
			break;
		case NorthWest:
			way = way.SouthEast;	
			break;
		case East:	
			way = way.West;	
			break;
		case North:
			way = way.South;		
			break;
		}

	}

	@Override
	protected void adjustEntity() {
		// TODO Auto-generated method stub
		switch(way) {       // changes x and y based on direction
		case South:
			y += spd ;	
			break;
		case West:	
			x -= spd ;
			break;
		case East:	
			x += spd ;
			break;
		case North:
			y -= spd ;	
			break;
		case SouthEast:
			y += spd;
			x += spd;
			break;
		case SouthWest:
			y += spd;
			x -= spd;
			break;
		case NorthEast:
			y -= spd;
			x += spd;
			break;
		case NorthWest:
			y -= spd;
			x -= spd;
			break;
		}

	}

	protected void checkEntity(int ax, int ay) {
		// TODO Auto-generated method stub
		if(x + rad>= ax) way = way.rightWall();               // will change direction based on which wall hit
     	if(y + rad>= ay) way = way.lowerWall();
		if(y <= 1 + rad) way = way.upperWall();
		if(x <= 1 + rad) way = way.leftWall();
		
	}
	@Override
	protected void drawEntity(ConsoleCanvas mc) {
		// TODO Auto-generated method stub
		mc.showEntity(x, y, rad, 'r'); // yellow drone is added

	}
	
	/**
	 * return string defining entity ... here as Drone
	 */
	protected String getStrType() {
		return "Runner ";
	}
	
	public boolean hitting(double ox, double oy, double or) {
		return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+(rad + 30))*(or+(rad + 30)); // makes the radius bigger so it moves before getting touched
	}
	
	public boolean hitting(Entity de) {
		
		return hitting(de.getX(), de.getY(), de.getRad());
	}
	
	
	

}
