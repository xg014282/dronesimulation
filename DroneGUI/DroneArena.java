package DroneSimulationGUI;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Random;

import ball2.MyCanvas;
import javafx.scene.paint.Color;

public class DroneArena implements Serializable{
	private int xSize, ySize;									// size of arena
	private int droneSize, blockSize;
	private Entity e;
	
	ArrayList<Entity> allEntities;
	
	
	/**
	 * construct arena of size xS by yS
	 * @param xS
	 * @param yS
	 */
	DroneArena(int xS, int yS){
		xSize = xS;
		ySize = yS;
		droneSize = 20;
		blockSize = 30;
		allEntities = new ArrayList<Entity>();
		allEntities.add(new Drone(xSize / 2, ySize / 2));
		allEntities.add(new Block( 120, 80));
	}
	
	/**
	 * return arena size in x direction
	 * @return
	 */
	public int getXSize() {
		return xSize;
	}
	/**
	 * return arena size in y direction
	 * @return
	 */
	public int getYSize() {
		return ySize;
	}
	
	
	/**
	 * draw all entities in the arena into interface bi
	 * @param bi
	 */
	public void drawWorld (ConsoleCanvas mc) {
		mc.clearCanvas();
	 	mc.setFillColour(Color.RED);
	 	for( Entity b : allEntities) {  // draws all the entities
	 		mc.setFillColour(Color.RED);
	 		b.drawEntity(mc); 
	 	}

	 }
	
	
	
	
	 /**
	  * see if entity can be placed there
	  * @param x
	  * @param y
	  */
	public boolean canPlace(int x, int y) {
		for(Entity a : allEntities) {
			if(withInX(x, a.getX(), a.getRad()) == true && withInY(y, a.getY(), a.getRad()) == true) { // sees if the place is within distance of another entity
				return false;
			}
				
		}
		return true;
	}
	/**
	  * see if x is within the entity
	  * @param x
	  * @param dx
	  * @param rad
	  */
	public boolean withInX(int x, double dx, double rad) {
		rad = rad + 10;
		if(x<= (rad + dx) && x> (dx-rad)) { // check within X radius
			return true;
		}
		return false;
	}
	/**
	  * see if y is within the entity
	  * @param x
	  * @param dx
	  * @param rad
	  */
	public boolean withInY(int y, double dy, double rad) {
		rad = rad + 10;
		if(y<= (rad + dy) && y> (dy-rad)) {  // check within Y radius
			return true;
		}
		return false;
	}
	
	/**
	 * add ball into the arena
	 */
	
	public void addDrone() {
		int x,y;
		
	    do {
	    	 x = ThreadLocalRandom.current().nextInt(droneSize, (xSize-droneSize));// generate random coordinates
			 y = ThreadLocalRandom.current().nextInt(droneSize, (ySize-droneSize));
	    }while(canPlace(x,y) != true);       // while there is space
		
			Drone d = new Drone(x, y);
			allEntities.add(d);                   // creates new drone and adds it to arraylist
		
		
		
	
	}
	/**
	 * add block into the arena
	 */
	
	public void addBlock() {
        int x,y;
		
	    do {
	    	 x = ThreadLocalRandom.current().nextInt(blockSize, (xSize-blockSize));// generate random coordinates
			 y = ThreadLocalRandom.current().nextInt(blockSize, (ySize-blockSize));
	    }while(canPlace(x,y) != true);            // while there is space
		
			Block d = new Block(x, y);
			allEntities.add(d);             // creates new block and adds it to arraylist
		
	}
	
	/**
	 * add runner into the arena
	 */
	
	
	public void addRunner() {
        int x,y;
		
	    do {
	    	 x = ThreadLocalRandom.current().nextInt(blockSize, (xSize-blockSize));// generate random coordinates
			 y = ThreadLocalRandom.current().nextInt(blockSize, (ySize-blockSize));
	    }while(canPlace(x,y) != true);            // while there is space
		
			Runner d = new Runner(x, y);
			allEntities.add(d);             // creates new block and adds it to arraylist
		
	}
	
	/**
	 * used to move entities and check if hit walls or other entities
	 * 
	 */
	public void updateArena() {
		for( Entity b : allEntities) {
	 		b.checkEntity(xSize, ySize);
	 		checkHit(b);
	 		b.adjustEntity();
	 	}
	}
	/**
	 * puts drone at that location
	 * @param x
	 * @param y
	 * 
	 */
	public void putDrone(int x, int y) {
		allEntities.get(0).setXY(x, y);
	}
	
	/**
	 * check if the entity  has been hit by another entity
	 * @param target	the target entity
	 * @return 	true if hit
	 */
	public void checkHit(Entity target) {
		
		for(Entity b : allEntities) {
			if(b.getID() != target.getID() && b.hitting(target.getX(),target.getY(),target.getRad())) {
				b.opposite();
				
			}
		     
		}
		
	}
	
	
	/**
	 * displays information on all entities
	 */ 
	public ArrayList<String> describeEntities() {
		ArrayList<String> ans = new ArrayList<String>();
		for (Entity d : allEntities)  ans.add(d.toString());	// displays entity info	
		return ans;
	}
	
}
