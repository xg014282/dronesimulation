package DroneSimulationGUI;

import java.io.Serializable;

public abstract class Entity implements Serializable {
	protected double x, y, spd, rad; // x and y of  entity, speed and radius
	private static int EntityCount = 0;
	protected int EntityID;
	
	Direction way; // direction of entity
	
	/**
	 * construct a entity with id and direction set
	 * @param ix
	 * @param iy
	 */
	Entity (double ix, double iy){
		x = ix;
		y = iy;
		EntityID = EntityCount++;
		way = way.getRandomDirection();
		
	}
	/**
	 * return x position
	 * @return
	 */
	public double getX() { return x; }
	/**
	 * return y position
	 * @return
	 */
	public double getY() { return y; }
	/**
	 * return radius of entity
	 * @return
	 */
	public double getRad() { return rad; }
	/**
	 * return ID of enetity
	 * @return
	 */
	public double getID() { return EntityID; }
	/** 
	 * set the entity at position nx,ny
	 * @param nx
	 * @param ny
	 */
	public void setXY(double nx, double ny) {
		x = nx;
		y = ny;
	}
	/** 
	 * check if entity is there
	 * @param nx
	 * @param ny
	 */
	public boolean isHere (int nx, int ny) {
		if(x == nx && y == ny) {
			return true;
		}
		else {
			return false;
		}
			
	}
	/**
	 * abstract method changing the entity's direction when colliding
	 */
	protected abstract void opposite();
	
	/**
	 * abstract method for adjusting a entity
	 */
	protected abstract void adjustEntity();
	
	/**
	 * abstract method for checking if entity is hitting walls
	 * @param ax
	 *  @param y
	 */
	protected abstract void checkEntity(int ax, int ay);
	
	
	/**
	 * abstract method for drawing entity
	 * @param mc
	 */
	protected abstract void drawEntity(ConsoleCanvas mc);
	
	/**
	 * return string defining ball ... here as entity
	 */
	protected String getStrType() {
		return "Entity ";
	}
	
	
	/**
	 * abstract method for drawing string
	 * 
	 */
	public  String toString() {
		return getStrType()+ EntityID + " at "+ Math.round(x) + ", "+ Math.round(y);
	};
	
	/**
	 * is ball at ox,oy size or hitting this entity
	 * @param ox
	 * @param oy
	 * @param or
	 * @return true if hitting
	 */
	public boolean hitting(double ox, double oy, double or) {
		return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+rad)*(or+rad);
	}
	/** is entity hitting the other entity
	 * 
	 * @param de - the other entity
	 * @return true if hitting
	 */
	public boolean hitting(Entity de) {
		return hitting(de.getX(), de.getY(), de.getRad());
	}
	
	
}
