package DroneSimulation;

public class ConsoleCanvas {
	private int cx, cy;
	private char [][] size;
	
	
 	ConsoleCanvas(int x, int y){
		cx =x;
		cy =y;
		size = new char [x][y];
		// outputs the array , makes sure borders are #
		for(int i =0; i < x; i++) {
			for(int j =0; j< y; j++) {
				size[i][j] = ' ';
				if(i == 0) size[i][j] = '#';
				if(j == 0)size[i][j] = '#';
				if (i == x -1)size[i][j] = '#';
				if (j == y -1)size[i][j] = '#';
				
					
				
				
			}
		}
		
	}
 	
 
 	
 	
	//prints out  the canvas
 	public String toString() {
 		String canvas = " ";
 		for(int i =0; i < size.length; i++) {
 			canvas = canvas + '\n';
			for(int j =0; j< size[i].length; j++) {
				canvas = canvas + size[i][j] + " " ;
			}
		}
 		
 		
 		return canvas;
 	}
 	
 
 	// used to show the drones on the canvas
	public void showIt(int x,int y, char d) {
		size[x][y] = d;
	}
	

}
