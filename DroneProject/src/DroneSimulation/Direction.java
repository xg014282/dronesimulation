package DroneSimulation;

import java.util.Random;

public enum Direction {

	North, East, South, West;
	//chooses the next direction
	public Direction next() {
		return values()[(ordinal() + 1) % values().length];
	}
	// gets a random direction
	public static Direction getRandomDirection() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
	
}
