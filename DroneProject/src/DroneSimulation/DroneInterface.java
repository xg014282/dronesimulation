package DroneSimulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class DroneInterface {
	private Scanner s;								// scanner used for input from user
    private DroneArena myArena;	
    ArrayList<Integer> info;
    String size;
	Scanner userIn;
	  public DroneInterface() {
	    	 s = new Scanner(System.in);// set up scanner for user input
	    	 myArena = new DroneArena (10,10);
	    	 info = new ArrayList<Integer>();
	    	 
	    	
	    	 	
	    	 
	    	
	        char ch = ' ';
	        do {
	        	System.out.print("Enter (A)dd drone, get (I)nformation, (D)isplay arena, (M)ove Drones, (N)ew Arena , (S)save file, (R)read save or e(X)it > ");
	        	ch = s.next().charAt(0);
	        	s.nextLine();
	        	switch (ch) {
	    			case 'A' :
	    			case 'a' :
	        					myArena.addDrone();	// add a new drone to arena
	        					break;
	        		case 'I' :
	        		case 'i' :
	        					System.out.print(myArena.toString());
	        					
	            				break;
	        		case 'D' :
	        		case 'd' :
	        					doDisplay();
	        			        
	        	    	        break;
	        		case 'm':
	        		case 'M':
	        			     for(int i = 0; i < 10; i ++) {
	        			    	 move();
	        			    	 wait(200);
	        			     }
	        		           
	        				   
	        			       break;
	        		case 'n':
	        		case 'N':
	        			      userInput();
	        			      break;
	        		case 's':
	        		case 'S':
	        				 clearFile();
	        			     saveFile();
	        			     break;
	        		case 'r':
	        		case 'R':
	        			     readFile();
	        			     loadSave();
	        			     
	        			     break;
	        		case 'x' : 	ch = 'X';				// when X detected program ends
	        					break;
	        	}
	    		} while (ch != 'X');						// test if end
	        
	       s.close();									// close scanner
	    }
	    //prints the arena
	    void doDisplay() {

			// Console canvas will be + 2 bigger so it can create border
			ConsoleCanvas canvas = new ConsoleCanvas(myArena.getX()+2, myArena.getY()+2);
			// call showDrones suitably
			myArena.showDrones(canvas);
			// then use the ConsoleCanvas.toString method
			System.out.println(canvas.toString());
		}
	    
	    // moves all the drones
	    void move() {
	    	ConsoleCanvas canvas = new ConsoleCanvas(myArena.getX()+2, myArena.getY()+2);
	    	myArena.moveAllDrones(myArena);
	    	myArena.showDrones(canvas);
	    	
	    	System.out.println(canvas.toString());
	    	System.out.print(myArena.toString());
	    	
	    }
	    
	    //delay used for animation
	    public static void wait(int ms)
	    {
	        try
	        {
	            Thread.sleep(ms);
	        }
	        catch(InterruptedException ex)
	        {
	            Thread.currentThread().interrupt();
	        }
	    }
	    // makes a new arena using userinput
	    public void userInput() {
	    	try {
	    		userIn = new Scanner(System.in);	
	    		System.out.print("What do you want the x and y of the arena to be?");
	    		size =  userIn.nextLine();
	    		StringSplitter me = new StringSplitter(size," ");
	    		String [] UserArena = me.getStrings();
	    		int x = Integer.parseInt(UserArena [0]);
	    		int y = Integer.parseInt(UserArena [1]);
	    		myArena = new DroneArena(x,y);
	    	} catch (Exception e) {
	    		System.out.println("Invalid size entered, please enter size in form of 'x y'");
	    	}
	    
	    }
	    
	    // saves the arena information into a file called save.txt
	    public void saveFile() {
			//arena size
			int sx = myArena.getX();
			int sy = myArena.getY();
			int [] arenaData = {sx,sy};
			File outFile = new File("save.txt");
			try {
			FileWriter outFileWriter = new FileWriter ( outFile );
			PrintWriter writer = new PrintWriter(outFileWriter);
			writer.println(sx);
			writer.println(sy);
			for(Drone d :myArena.drones) {
				writer.println(d.getX());
				writer.println(d.getY());
			}
		
			writer.close();
			} catch (FileNotFoundException e) {
			e.printStackTrace();
			} catch (IOException e) {
			e.printStackTrace();
			}
			
			System.out.println("file is saved");
			
					
		}
	    
	    // make sure data is not overlapped
	    public void clearFile() {
	    	File file = new File("save.txt");
	    	if(file.exists()){
	    	    file.delete();
	    	}
	    }
	
      // reads file and stores it in a arraylist
	    public void readFile() {
	    	
	    	//set up file and stream
	    	try {
	    	      File myObj = new File("save.txt");
	    	      Scanner myReader = new Scanner(myObj);
	    	      while (myReader.hasNextLine()) {
	    	        String data = myReader.nextLine();
	    	        int i = Integer.parseInt(data);
	    	        info.add(i);
	    	      }
	    	      
	    	      myReader.close();
	    	     
	    	    } catch (FileNotFoundException e) {
	    	      System.out.println("An error occurred.");
	    	      e.printStackTrace();
	    	    }
	    	
	    	
	    }
	    
	    // translates the arraylist into a arena
	    public void loadSave() {
	    	
	    	myArena = new DroneArena(info.get(0),info.get(1));
	    	
	    	for(int i = 2; i < info.size(); i+=2) {
	    		Drone d = new Drone(info.get(i),info.get(i+1));
	    		myArena.drones.add(d);
	    	}
	    	
	    	
	    	
	    }
	    
	    
		public static void main(String[] args) {
		       
			// just call the interface
			DroneInterface r = new DroneInterface();
			
		}
	
}
