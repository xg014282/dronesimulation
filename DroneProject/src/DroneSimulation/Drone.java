package DroneSimulation;

public class Drone {
	private int dx,dy, DroneId;
	
	private static int DroneCount = 1;
	Direction way;
	Drone(int x, int y){
		dx = x;
		dy = y;
		way = way.getRandomDirection();
		DroneId  = DroneCount ++;
	}
	//getters
	public int getX() {
		return dx;
	}
	
	public int getY() {
		return dy;
	}
	
	public int GetDroneId() {
		return DroneId;
	}
	// prints out the drone's info
	public String toString() {
		String position = "Drone " + DroneId +" is  at "+ dx + "," +dy + " and has direction " + way;
		return position;
	}
	
	// used to check if a drone is there
	public boolean isHere (int sx, int sy) {
		if(dx == sx && dy == sy) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	// displays the drone on the canvas
	public void displayDrone(ConsoleCanvas c) {
		c.showIt(dx, dy, 'D');
	}
	
	// moves the drone based on direction
	public void tryToMove(DroneArena d) {
	
		for(int i = 0; i < 4; i++) {
			int nx = dx;
			int ny = dy;
			switch(way) {
			case South:
				nx = nx + 1;
			    break;
			case West: 
				ny = ny - 1;
				break;
			case East:
				ny = ny + 1;
				break;
			case North:
				nx = nx -1;
				break;
				
			}
			if(d.canMoveHere(nx, ny) == true) {
				dx = nx;
				dy = ny;
				
				break;
			}
			
			nextDirection();
			
		}
			
		
		
		
		
	}
	
	
	// changes the drone's direction the next one
	public void nextDirection() {
		way = way.getRandomDirection();
	}
}
