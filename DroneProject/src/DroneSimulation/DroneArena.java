package DroneSimulation;

import java.util.ArrayList;
import java.util.Random;

public class DroneArena {
	private int ax, ay;
	Random ranNum;
	ArrayList<Drone> drones;
	
	DroneArena(int x, int y){
		ax = x;
		ay = y;
		ranNum = new Random();
		
		drones = new ArrayList<Drone>();
		
	}
	// getters
	public int getX() {
		return ax;
	}
	public int getY() {
		return ay;
	}
	
	
	//used to see if a drone is at those coordinates
	public Drone getDroneAt(int x, int y) {
		Drone d = null;
		for(Drone a : drones) {
			if(a.isHere(x,y) == true)
				d = a;
		}
		return d;
	}
	//adds a drone to the arena
	public void addDrone() {
		int x,y;
		if(drones.size() < ax * ay) {
			do {
				x = ranNum.nextInt(ax) + 1;
				y = ranNum.nextInt(ay) + 1;
			} while(getDroneAt(x,y) != null);
			Drone d = new Drone(x,y);
			drones.add(d);
			
		}else {
			System.out.println("The arena is full!");
		}
				
	}
	
	// makes the drones come up on canvas
	public void showDrones (ConsoleCanvas c) {
		for(Drone d : drones) {
			d.displayDrone(c);
		}
	}
		
	// prints out the info of the arena and the drones
	public String toString() {
		String arenaInfo = "";
		arenaInfo = arenaInfo + "The arena is size "+ax +"," + ay;
		for(int i = 0; i < drones.size(); i++) {
			arenaInfo = arenaInfo + "\n" + drones.get(i).toString() + "\n";
		}
			
		return arenaInfo;
	}
		
	// moves all the drones
	public void moveAllDrones(DroneArena a) {
			
		for(Drone d : drones) { 
			d.tryToMove(a);
		}
	}
		
		// see if the drone can move there
	public boolean canMoveHere(int x, int y) {
			
		for(int i =0; i < drones.size(); i++) {
			if(drones.get(i).isHere(x,y) == true) {
				return false;
			}
			
		}
			// check if its the border
		if(x == 0 || y == 0) {
			return false;
		}
		if ( x== getX()+1 || y == getY()+ 1) {
			return false;
		}
			
		return true;
			
			
	}
  
}
